--- 
title: "Single cell RNA-seq tutorial"
author: "Eddie Cano Gamez and Marta Pérez Alcántara"
date: "2021-03-09"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
description: "This is a small scRNA-seq tutorial. The output format for this example is bookdown::gitbook."
---


# Prerequisites

In this session, we will go through some of the most common analyitcal approaches for visualising a scRNA-seq data set and identifying groups of cells. We will use a data set containing ~3,000 peripheral blood mononuclear cells (PBMCs) made publicly available by 10X.

You'll need to [download](https://cf.10xgenomics.com/samples/cell/pbmc3k/pbmc3k_filtered_gene_bc_matrices.tar.gz) the example dataset and extract it.

This session is based on the Guided Clustering in Seurat tutorial, designed by Rahul Satija's group, which you can access via the [following link](https://satijalab.org/seurat/archive/v3.2/pbmc3k_tutorial.html).

Let's get started!


## Load R libraries
We'll begin by loading Seurat (note that this tutorial uses v3.2.1), as well as well as a few other libraries that should give us enough functionality to complete all of our analysis. We are also using R version 3.6.x. Check your R and library versions running *sessionInfo()*.


```r
# Sys.setenv("R_REMOTES_NO_ERRORS_FROM_WARNINGS" = "true")
# # Install these if you haven't done so already
# install.packages("devtools")
# devtools::install_github(repo = 'satijalab/seurat', ref ="b56d194939379460db23380426d3896b54d91ab6", force = T) # seurat commit for v.3.2.2
# install.packages("tidyverse")
# install.packages("future")
# install.packages("clustree")
# if (!requireNamespace("BiocManager", quietly = TRUE))
#     install.packages("BiocManager")
# BiocManager::install("limma")
# devtools::install_github("thomasp85/patchwork")
# devtools::install_github('chris-mcginnis-ucsf/DoubletFinder')

# For all parts
library(Seurat)
library(limma)
library(tidyverse) # For ggplot2 and easy manipulation of data
library(patchwork) # To combine plots
library(future) # For paralellization
library(DoubletFinder) # For finding  doublets
library(clustree) # To select clustering resolution
options(stringsAsFactors = FALSE) # Set this to deactivate the automatic read-in of characters as factors
```


We will also set point R to the correct version of Python to use, if needed. This will be important when using the clustering functions. 

```r
# This is for building the book, no need to run it
install.packages(c("bookdown", "knitr"))
library(bookdown)
library(knitr)
# Ignore this line, which will point R to the correct python version for Leiden clustering
Sys.setenv(RETICULATE_PYTHON = "./python/bin/python")
reticulate::py_config()
```








<!--chapter:end:index.Rmd-->

# Reading the data: UMIs vs counts {#intro}


## scRNA-seq technologies

There are two ways to amplify the mRNA transcripts: either creating full-length cDNA or just the 3' or 5' end. In the former, one gets whole read counts at the end of the mapping process, while the latter adds a Unique Molecular Identifier (UMI) to the sequence for more accurate quantification of the reads.

The following figure lists some of the technologies available: 

<div class="figure" style="text-align: center">
<img src="data/figures/technologies-papalexi.png" alt="Some scRNA-seq alternatives. From Papalexi and Satija, 2017." width="80%" />
<p class="caption">(\#fig:unnamed-chunk-5)Some scRNA-seq alternatives. From Papalexi and Satija, 2017.</p>
</div>
Each technology has its own set of advantages and drawbacks. The following table may help you choose the best method for your experiment.

<div class="figure" style="text-align: center">
<img src="data/figures/technologies-See.png" alt="Summary of some scRNA-seq methods. From See _et al._, 2018." width="80%" />
<p class="caption">(\#fig:unnamed-chunk-6)Summary of some scRNA-seq methods. From See _et al._, 2018.</p>
</div>

In general, UMI-based methods like 10X are cheaper and allow for more accurate quantification as it tags each mRNA before PCR amplification, distinguishing PCR duplicates from reads that are truly more abundant. On the other hand, full-length methods like SMART-seq allow any analysis that requires the full length of the transcript (e.g. isoform quantification) and improves methods that benefit from including all exons (e.g. deconvolution of the identity of pooled samples).

In terms of mapping and quantification of the reads, a popular method of analysis of 10X data is CellRanger. You can find here extensive documentation of how to use it, and it can also analyse at the same time the results of CRISPR guide capture or protein barcode experiments. 

A very thorough explanation of widely used mapping methods for full-length data can be found in the [Sanger course](https://scrnaseq-course.cog.sanger.ac.uk/website/processing-raw-scrna-seq-data.html).


## Reading in the data

We can now load the scRNA-seq data set and use it to create a Seurat object which we can base our analysis on.

You can use Seurat's function `Read10X()` to read the typical output files from cellRanger (one file containing cell barcodes, one containing gene names and information, and one with the full expression matrix). Make sure that `data.dir` points to the directory where you have saved the data.

```r
# Load the PBMC dataset
pbmc.data <- Read10X(data.dir = "data/raw_counts/filtered_gene_bc_matrices/hg19/")
```

You could read in full length data using the base R read functions such as `read.delim()`, as detailed in the [Sanger course](https://scrnaseq-course.cog.sanger.ac.uk/website/tabula-muris.html).

<br>

<details>
  <summary>**10X with antibody or CRISPR guide capture**</summary>


If your CellRanger output contains data from antibody or CRISPR guide capture, apart from the regular gene expression data, you could create your Seurat object like this:



```r
pbmc<- CreateSeuratObject(counts = pbmc.data$`Gene Expression`, project = "pbmc3k")
pbmc[["gRNA"]] <- CreateAssayObject(counts = pbmc.data$`CRISPR Guide Capture`)
```
</details>
\ 

\n
We can now use this data to construct a Seurat object. This object is the data structure on which all the following analysis will be based.

When creating this object, we make sure to add a project name and we may filter out any low quality genes (observed in less than 3 cells). We could also remove low qualilty cells with the parameter `min.features`, but we'll have a look first at the general quality of the cells in the next section. 

```r
# Initialize the Seurat object with the raw (non-normalized data).
pbmc <- CreateSeuratObject(counts = pbmc.data, 
                           min.cells = 3, 
                           project = "pbmc3k")
```

```
## Warning: Feature names cannot have underscores ('_'), replacing with dashes ('-')
```
Previewing the Seurat object reveals that it contains RNA measurements for 2.7k cells and ~13k genes.

```r
pbmc
```

```
## An object of class Seurat 
## 13714 features across 2700 samples within 1 assay 
## Active assay: RNA (13714 features, 0 variable features)
```
You can have a closer look at the internal structure with `str(pbmc)`.

## Analysis of the sparse matrix format
But let's take a moment to study how our data is stored within the Seurat object. We can use the squared brackets [] to access specific rows (genes) and columns (cells) in the matrix, either via their name or their index. This works in the same way as when you are slicing a data frame.

For example, let's examine the expresison of CD3, CD20 (MS4A1), and CD14 in the first 30 cells.

```r
# Lets examine a few genes in the first thirty cells
pbmc.data[c("CD3D", "MS4A1", "CD14"), 1:30]
```

```
## 3 x 30 sparse Matrix of class "dgCMatrix"
```

```
##    [[ suppressing 30 column names 'AAACATACAACCAC-1', 'AAACATTGAGCTAC-1', 'AAACATTGATCAGC-1' ... ]]
```

```
##                                                                    
## CD3D  4 . 10 . . 1 2 3 1 . . 2 7 1 . . 1 3 . 2  3 . . . . . 3 4 1 5
## MS4A1 . 6  . . . . . . 1 1 1 . . . . . . . . . 36 1 2 . . 2 . . . .
## CD14  . .  . . . . . . . . . . . 4 . . . . 1 .  . . . . 2 . . . . .
```

You'll notice that most entries in the matrix contain dots (.). This is the way Seurat indicates zero values. This is because Seurat makes use of a data structure called a sparse Matrix object. Instead of allowing all of those zeros to take up unecessary space, they are not stored in memory at all (this is made possible via an underlying structure where entries in the matrix are represented using nodes and links). This reduces memory usage substantially.

For example, if our data were stored as a normal matrix it would take up 0.7 Gb of RAM:

```r
dense.size <- object.size(as.matrix(pbmc.data))
dense.size
```

```
## 709591472 bytes
```

In contrast, using the Sparse Matrix format reduces this to 0.02 Gb.

```r
sparse.size <- object.size(pbmc.data)
sparse.size
```

```
## 29905192 bytes
```

Turns out this is 24 times more efficient!

```r
as.integer(round(dense.size/sparse.size))
```

```
## [1] 24
```

Now that we know how the data is stored behind the scenes, let's get going with some analyses.

<!--chapter:end:01-intro.Rmd-->

# Quality Control {#qc}

Here we will perform an initial quality control of the 10X data. This is important to reduce the impact of technical factors and maximize our biological insights down the line.


## General metrics

First we will look at the general quality metrics: percentage of mitochondrial genes, number of genes and UMI counts. 

Let's first estimate the proportion of reads in each cell which map to mitochondrial genes (i.e. mitochondrial content). Mitochondrial content is a useful quality metric because if cells lysed during the experiment due to poor technical conditions, then we will see that most cytoplasmic RNA leaked out of the cell and is not captured, while mitochondrial RNA was kept (since it is protected by an additional layer of membrane). This is seen as an unusually large proportion of reads mapping to the mitochondrial genome.

We can estimate the mitochondrial content by counting all reads which map to genes starting with "MT-" (hence the regular expression ^MT-, where the carat indicates the beginning of the word), the convention for naming mitochondrial genes. We will add this information to a new data slot in the metadata object called "percent.mt".



```r
# Adding the percentage of mitochondrial genes to the metadata table
pbmc[["percent.mt"]] = PercentageFeatureSet(pbmc, pattern = "^MT-")
```

You can access the whole metadata with the '@' operator:

```r
head(pbmc@meta.data)
```

```
##                  orig.ident nCount_RNA nFeature_RNA percent.mt
## AAACATACAACCAC-1     pbmc3k       2419          779  3.0177759
## AAACATTGAGCTAC-1     pbmc3k       4903         1352  3.7935958
## AAACATTGATCAGC-1     pbmc3k       3147         1129  0.8897363
## AAACCGTGCTTCCG-1     pbmc3k       2639          960  1.7430845
## AAACCGTGTATGCG-1     pbmc3k        980          521  1.2244898
## AAACGCACTGGTAC-1     pbmc3k       2163          781  1.6643551
```
For example, here the first cell has ~3% of reads mapping to mitochondrial genes.

You can check and add any per-cell information you want to the metadata table in a few different ways:


```r
# Retrieve specific values from the metadata
pbmc$nCount_RNA
pbmc[[c("percent.mt", "nFeature_RNA")]]

# Another way to add metadata information
pbmc@meta.data$percent.mt= PercentageFeatureSet(pbmc, pattern = "^MT-")
```
Other important metrics in this table are nFeature (number of genes detected per cell) and nCount (number of UMIs detected per cell). These can tell us something about the size and total RNA content of each cell.

Let's visualise all of these metrics using Seurat's violin plot function.

```r
#Visualize QC metrics as a violin plot
VlnPlot(pbmc, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3) 
```

<img src="scRNA-seq_workshop_2021_files/figure-html/unnamed-chunk-18-1.png" width="95%" style="display: block; margin: auto;" />
And see their relationship with scatterplots:

```r
# FeatureScatter is typically used to visualize feature-feature relationships, but can be used for anything calculated by the object, i.e. columns in object metadata, PC scores etc.

plot1 = FeatureScatter(pbmc, feature1 = "nCount_RNA", feature2 = "percent.mt") + NoLegend()  +
  ylab("% of mitochondrial genes") +
  xlab("UMI counts") + 
  geom_hline(yintercept = 5) 

plot2 = FeatureScatter(pbmc, feature1 = "nCount_RNA", feature2 = "nFeature_RNA") + NoLegend() +
  ylab("Number of genes") +
  xlab("UMI counts") + 
  geom_hline(yintercept = 200) 


plot1 + plot2 
```

<img src="scRNA-seq_workshop_2021_files/figure-html/unnamed-chunk-19-1.png" width="95%" style="display: block; margin: auto;" />
The black lines indicate usual quality thresholds, by which cells with less than 200 genes (which can indicate empty droplets) and over 5% mitochondrial genes are removed. 

<details>
  <summary>**10X with antibody or CRISPR guide capture**</summary>


With CRISPR guide capture you could also include the metrics *nFeature_gRNA* (number of guides detected per cell) and *nCount_gRNA* (gRNA counts detected per cell). With the following code you can plot the relationship between RNA and gRNA counts.



```r
FeatureScatter(seurat_object, feature1 = "nCount_RNA", feature2 = "nCount_gRNA") +
  ylab("Barcode counts") +
  xlab("UMI counts")
```
<img src="data/figures/PTK2B_lib1_lib2_metrics_scatter.png" width="80%" style="display: block; margin: auto;" />
Keep in mind that these metrics are not exactly the same than those from the *called* guide in each cell. The called guide (if any) will be the one that is present significantly above background. Seurat metadata will report the metrics for all guides detected in each cell. If you wanted to add the metrics only for what is called, you'll need to add it to the metadata manually. The barcode calling results from CellRanger are in `crispr_analysis/protospacer_calls_per_cell.csv`.

</details>
\  

There are a few things to consider when selecting thresholds for these quality control measurements:

* Cell doublets (or multiplets) often have a higher gene count. Although in the Seurat tutorial they observe which cells are high gene number outliers and remove those, it is controversial to set an arbitrary higher threshold of genes, particularly in heterogeneous populations. We'll have a look at other ways to remove likely doublets.
* Low quality and stressed/dying cells have a higher than usual amount of mitochondrial genes. But the threshold of "normal" percentage of mitochondrial genes varies between species and cell types (see the following plot). You can also inspect apoptosis markers and crosscheck their expression with that of mitochondrial genes to ensure you are removing the right population.

<div class="figure" style="text-align: center">
<img src="data/figures/Osorio-mit.png" alt="Mitochondrial proportion in some human tissues. From Osorio and Cai, 2020." width="80%" />
<p class="caption">(\#fig:unnamed-chunk-22)Mitochondrial proportion in some human tissues. From Osorio and Cai, 2020.</p>
</div>


## Doublets and multiplets

Sometimes two or more cells will be processed together when preparing the libraries for sequencing. These cells can cause problems in differential expression and other analyses down the line, and can be confused for intermediate populations that don't really exist. In pooled experiments where cells that have been barcoded or whose genotypes can be deconvoluted (with Demuxlet, Vireo, etc.), doublets can be straightforwardly detected if they are a combination of distinct donors. But what about doublets from the same donors, or experiments where we don't have this deconvolution information?

There are several packages in R to remove cells that we suspect that are not singlets e.g. DoubletFinder, scDblFinder, DoubletDecon or the computeDoubletDensity() function from scran. We'll use DoubletFinder because it is widely adopted and it easily integrates within the typical Seurat workflow.

It is important to remember that DoubletFinder is sensitive to heterotypic doublets -- i.e., doublets formed from transcriptionally-distinct cell states. It is therefore helpful to identify technical "intermediate" cell states and remove them, aiding the interpretation of true biological transitions. On the other hand, it is insensitive to homotypic doublets -- i.e., doublets formed from transcriptionally-similar cell states. So those must be inferred with other methods (for example the aforementioned barcoding/deconvolution methods). 

It benefits from "true doublet" signals, for example barcoded samples that show double/multiple calls in CellRanger or deconvoluted cells from pooled experiments detected for example with Demuxlet or Vireo. Of course, those techniques can only detect doublets from different donors, and that's why it's useful to try to detect doublets in a different way.



DoubletFinder can be broken up into 4 steps:

1. It generates variable numbers of artificial doublets (pN) from pre-filtered scRNA-seq data and incorporates these into the Seurat object.

2. It then pre-processes merged real-artificial data using normalization and scaling (e.g. with SCTransform).

3. Performs PCA and uses the PC distance matrix to find each cell's proportion of artificial k nearest neighbors (pANN), across variable PC space neighborhood sizes (pK).This process is repeated for each pN and pK value, creating a list of pANN values. Optimal pK is then selected using BCmvn maximization, and this pK is applied to the full dataset. 

4. It estimates the number of total and heterotypic doublets from the Poisson doublet formation rate with and without homotypic doublet adjustment. Homotypic doublets are estimated as the sum of squared cell-type annotations or unsupervised clustering frequencies. Final doublet classifications are then made by rank-ordering and thresholding pANN according to these doublet number predictions, and doublets are then removed prior to subsequent downstream analyses.



<div class="figure" style="text-align: center">
<img src="data/figures/DoubletFinderOverview.png" alt="DoubletFinder algorithm overview. From DoubletFinder GitHub." width="95%" />
<p class="caption">(\#fig:unnamed-chunk-23)DoubletFinder algorithm overview. From DoubletFinder GitHub.</p>
</div>

As described above, DoubletFinder needs to perform filtering, normalization and scaling of the data, which will be described later in the tutorial. We won't run this next step as it can take a long time, but you can find the code below in case you're interested in applying it to your data.
<details>
  <summary>**Example code for running DoubletFinder**</summary>




```r
mark_doublets = function(seurat_object){
  
  message("Performing filter by number of genes and mitochondrial percentage.")
  seurat_object = subset(seurat_object, subset = nFeature_RNA > 200  & percent.mt < 5)
  message("Now the object has ", dim(seurat_object)[1], " genes and ", dim(seurat_object)[2], " cells.")
  
  # SCTransform
  # change the current plan to access parallelization
  plan("multiprocess", workers = 4)
  plan()
  # Give more memory to globals to prevent crashing by large Seurat objects
  options(future.globals.maxSize= 2097152000) # 2Gb
  
  seurat_object = SCTransform(seurat_object, verbose = FALSE)
  
  seurat_object = RunPCA(seurat_object, verbose = FALSE)
  
  seurat_object = FindNeighbors(seurat_object, dims = 1:30, verbose = FALSE)
  seurat_object = FindClusters(seurat_object, verbose = FALSE)
  
  seurat_object = RunUMAP(seurat_object, dims = 1:30, verbose = FALSE)
  
  sweep.res.list = paramSweep_v3(seurat_object, PCs = 1:10, sct = TRUE)
  sweep.stats = summarizeSweep(sweep.res.list, GT = FALSE)
  bcmvn = find.pK(sweep.stats)
  
  # This plot will help you find the correct value for pK
  ggplot(bcmvn,aes(x=pK,y=BCmetric, group = 1)) + geom_line() + geom_point()
  # select pK that maximizes the BCmetric - if there are two peaks with the same height, choose the one with lowest pK
  bcmvn$pK = as.numeric(as.character(bcmvn$pK))
  
  pk = bcmvn[which(bcmvn$BCmetric == max(bcmvn$BCmetric)),"pK"]
  
  message("Your best pK value is ", pk)

  # Homotypic doublet proportion estimation - from number of clusters
  annotations =  as.character(seurat_object$seurat_clusters)
  homotypic.prop <- modelHomotypic(annotations)   
  
  # Estimate number of total expected doublets
  # Assuming 2.07% doublet formation rate (10X rate for 2700 recovered cells) - tailor for your dataset
  expected_doublet_rate = 0.02
  nExp_poi = round(expected_doublet_rate*nrow(seurat_object@meta.data)) 
  # Adjusting number based on homotypic doublet proportion expected
  nExp_poi.adj <- round(nExp_poi*(1-homotypic.prop))
  
  # All confidence doublets - not adjusted for homotypic doublets
  seurat_object.doublet <-
    doubletFinder_v3(
      seurat_object,
      PCs = 1:10,
      pN = 0.25,
      pK = pk,
      nExp = nExp_poi, 
      reuse.pANN = FALSE,
      sct = TRUE
    )
  
  # High confidence doublets - adjusted for homotypic doublets
  
  seurat_object.doublet <-
    doubletFinder_v3(
      seurat_object.doublet,
      PCs = 1:10,
      pN = 0.25,
      pK = pk,
      nExp = nExp_poi.adj, 
      reuse.pANN = colnames(seurat_object.doublet@meta.data)[ncol(seurat_object.doublet@meta.data)-1],
      sct = TRUE
    )
  
  # Integrate info all and high confidence
  seurat_object.doublet@meta.data$DF_confidence = "Singlet"
  seurat_object.doublet@meta.data[seurat_object.doublet[[colnames(seurat_object.doublet@meta.data)[ncol(seurat_object.doublet@meta.data) -
                                                                                                     2]]] == "Doublet" &
                                    seurat_object.doublet[[colnames(seurat_object.doublet@meta.data)[ncol(seurat_object.doublet@meta.data)-1]]] == "Doublet",
                                  "DF_confidence"] = "High_confidence_doublet"
  
  seurat_object.doublet@meta.data[seurat_object.doublet[[colnames(seurat_object.doublet@meta.data)[ncol(seurat_object.doublet@meta.data) -
                                                                                                     2]]] == "Doublet" &
                                    seurat_object.doublet[[colnames(seurat_object.doublet@meta.data)[ncol(seurat_object.doublet@meta.data)-1]]] == "Singlet",
                                  "DF_confidence"] = "Low_confidence_doublet"
  
  return(seurat_object.doublet)


}

pbmc.doublet = mark_doublets(pbmc)

# Saving only doublet information columns
write.csv(pbmc.doublet@meta.data[(ncol(pbmc.doublet@meta.data)-2):ncol(pbmc.doublet@meta.data)], 
          file = "data/results/pbmc_doublets.csv")
```

Now we can load the results and inspect the data:

```r
# Read in results from doublet calling
doublets = read.csv("data/results/pbmc_doublets.csv", row.names = 1)
# Add predicted doublets to metadata
pbmc@meta.data$DF_confidence = "Singlet"
pbmc@meta.data[rownames(doublets[doublets[ncol(doublets)] == "High_confidence_doublet",]),"DF_confidence"] = "High_confidence_doublet"
pbmc@meta.data[rownames(doublets[doublets[ncol(doublets)] == "Low_confidence_doublet",]),"DF_confidence"] = "Low_confidence_doublet"

message("There are ", sum(pbmc@meta.data$DF_confidence!="Singlet"), " predicted doublets in our data")
message("That's ", round((sum(pbmc@meta.data$DF_confidence!="Singlet")/nrow(pbmc@meta.data))*100), "% of our our data")


 calculate_SCT_PCA_UMAP_neighbors_clusters = function(seurat_object){
   
  seurat_object = SCTransform(seurat_object, verbose = FALSE)
  seurat_object <- RunPCA(seurat_object, verbose = FALSE)
  
  seurat_object <- FindNeighbors(seurat_object, dims = 1:30, verbose = FALSE)
  seurat_object <- FindClusters(seurat_object, verbose = FALSE)
  
  seurat_object <- RunUMAP(seurat_object, dims = 1:30, verbose = FALSE)
  
  
  
  return(seurat_object)
}

 # Saving in a new object so we don't disturb the old un-transformed, unfiltered one
pbmc.doublet = calculate_SCT_PCA_UMAP_neighbors_clusters(pbmc)

p = DimPlot(
  pbmc.doublet,
  label = F,
  group.by = "DF_confidence",
  cols = c(
    'Singlet' = '#28C2FF',
    'High_confidence_doublet' = '#BD6B73',
    'Low_confidence_doublet' = '#FFA630'
  ),
  order = c('High_confidence_doublet','Low_confidence_doublet' ) # Plot Doublets on top
) +
  ggtitle("DoubletFinder doublets") 
```
If you have distinct samples, either barcoded or deconvoluted, that can be used as "ground truth":

- You can find the pK value based on these cells, and see how it agrees with the value calculated with the simulation. If they don't agree, or if the predicted doublets don't overlap, it might be because the "ground truth" doublets are more transcriptomically similar than what DoubletFinder can classify. The recommended aproach is to remove the "ground truth" doublets only, or those AND the ones predicted by DoubletFinder.
- Keep in mind that the expected doublet rate (that we chose here to be 0.02 or 2% based on an extrapolation from the [10X guide](https://assets.ctfassets.net/an68im79xiti/6Il3RTklA509adfx7KJrNR/c5217b1adf7a406e67c53ce8f895b638/CG000208_ChromiumNextGEMSingleCellV_D_J_ReagentKit_v1.1_FeatureBarcodingtechnology_RevF.pdf)) depends on the number of distinct cells sequenced: so if there are two donors, the probability of multiplets would be 1/2, if 3 then 1/3 and so on. So for 10,000 cells we usually expect 7.6% doublets, but if there are 7 donors in the pool then the rate would be 7.6/7=1.09%, or a 0.01 expected doublet rate.

You can find the code to calculate the "ground truth" pK down below:

```r
# from sweep.res.list calculation

gt.calls <- seurat_object@meta.data[rownames(sweep.res.list[[1]]), "GT"].   ## GT is a vector containing "Singlet" and "Doublet" ground truth calls
sweep.stats <- summarizeSweep(sweep.res.list, GT = TRUE, GT.calls = gt.calls)
bcmvn<- find.pK(sweep.stats)
```

 
 

 </details>
\ 
The final UMAP plot of the doublets for the PBMC dataset looks like this:
<img src="data/figures/PBMC_UMAP_doublets.png" width="95%" style="display: block; margin: auto;" />
As you can see this is a very clean dataset, and we can't identify many doublets (only 53 cells, around 2%). A "dirtier" dataset would look like this:



<div class="figure" style="text-align: center">
<img src="data/figures/DFkidney_low.vs.high.png" alt="DoubletFinder classification on UMAP from kidney cells. From DoubletFinder GitHub." width="95%" />
<p class="caption">(\#fig:unnamed-chunk-28)DoubletFinder classification on UMAP from kidney cells. From DoubletFinder GitHub.</p>
</div>


## Filtering

Now you can subset the cells to those that pass the selected quality control thresholds. We have seen that our data set is fairly clean, with most cells having a low mitochondrial content. However, to improve its quality even further let's only keep cells with less than 5% of reads mapping to the mitochondria. Let's also remove any cells with less than 200 genes (these might simply be empty droplets or lysed cells). We aren't including the doublet information here so you don't have to perform the calculations (and because there are not many in this dataset), but you could subset the singlets only by including `Doublet_DF!="Singlet"` in your filters. 

```r
filter_seurat = function(seurat_object){
  
  message("Performing filter by number of genes and mitochondrial percentage.")
  seurat_object = subset(seurat_object, subset = nFeature_RNA > 200  & percent.mt < 5)
  message("Now the object has ", dim(seurat_object)[1], " genes and ", dim(seurat_object)[2], " cells.")

  
  return(seurat_object)
}

pbmc = filter_seurat(pbmc)
```

```
## Performing filter by number of genes and mitochondrial percentage.
```

```
## Now the object has 13714 genes and 2643 cells.
```




<!--chapter:end:02-qc.Rmd-->

# Normalisation and batch correction {#methods}

Here we will have a look at the most widely used methods of normalisation and scaling, and also how and when to perform batch correction of different covariates in our data.

## Normalisation and scaling

Both bulk and single cell RNA-seq need to correct for differences in sequencing depth between samples or cells to make biologically-sound comparisons of expression that are not driven by this technical factor. In addition, appropriate modelling of the gene expression variance is needed to apply the correct statical tests for differential expression, and to prevent an inflation of significance for genes that have either very high or very low counts. All these issues are exacerbated in scRNA-seq due to the sparsity of the data caused by the high number of technical gene drop-outs (that is, genes that are expressed but are not sequenced by chance - also called zero-inflation).

Normalisation methods are applied to make comparison between cells. Some simple normalisation methods are fairly straightforward: count per million (CPM), reads per kilobase per million (RPKM), etc. Some more sophisticated methods come from bulk RNA-seq, like trimmed mean of M values (TMM) (Robinson and Oshlack, 2010), or DESeq (Anders and Huber, 2010). Nevertheless, these methods assume there's a homogeneous cell population, which is often not the case, and don't take into account the drop-outs. 

There are several packages that try to correct for all single-cell specific issues and perform the most adequate modelling for normalisation. Some popular ones are [scran](https://bioconductor.org/packages/release/bioc/html/scran.html), [SCnorm](https://www.bioconductor.org/packages/release/bioc/html/SCnorm.html), Seurat's LogNormalize(), and the new normalisation method from Seurat: SCTransform(). 

Scaling allows for comparison between genes, within and between cells. Generally, scaling methods center the already-normalised data (substracting the mean expression of the gene across all cells) and generate Z-scores (dividing each centered datapoint by the standard deviation of the gene across all cells). This is often done for visualization purposes (e.g. in heatmaps). scale() in base R and ScaleData() in Seurat do only this step, but the SCTransform() algorithm performs a slightly different scaling (using Pearson residuals instead of Z-scores) after normalisation in the same function. 

### SCTransform()

This method uses regularized negative binomial regression to remove the variation due to sequencing depth (total UMIs per cell). It then corrects the variance by pooling information across genes with similar abundances. It gives as a result Pearson residuals, and in the same way than Z-scores, positive residuals for a gene in a certain cell indicate that we observed more UMIs than expected given the gene’s average expression in the population and sequencing depth, while negative residuals indicate the opposite (as described in the SCTransform vignette). This scaling ensures that highly expressed genes do not dominate downstream analyses.


```r
pbmc <- SCTransform(pbmc, verbose = FALSE)
```

The results of sctransfrom are stored in the “SCT” assay. 


```r
pbmc[["SCT"]]
```

```
## Assay data with 12553 features for 2643 cells
## Top 10 variable features:
##  S100A9, GNLY, LYZ, S100A8, NKG7, FTL, GZMB, IGLL5, CCL5, FTH1
```

`pbmc[["SCT"]]@scale.data`  contains the residuals (normalized values), and is used directly as input to PCA. To save memory, we store these values only for the top 3000 variable genes, by setting the return.only.var.genes = TRUE by default in the SCTransform() function call. These variable genes are used in the calculation of principal components. You can include more than 3000 genes (which will take more memory), but it doesn't affect much the results.

To assist with visualization and interpretation. we also convert Pearson residuals back to ‘corrected’ UMI counts. You can interpret these as the UMI counts we would expect to observe if all cells were sequenced to the same depth. 
The ‘corrected’ UMI counts are stored in `pbmc[["SCT"]]@counts` . 

We store log-normalized versions of these corrected counts in  `pbmc[["SCT"]]@data` , which are very helpful for visualization.
You can use the corrected log-normalized counts for differential expression and integration. 

<details>
  <summary>**What if this takes too long or I run out of memory?**</summary>

If you have a big dataset or want to include more variable genes this step can take longer. You can improve this accessing parallelization with the [future](https://cran.r-project.org/web/packages/future/index.html) library. You can increase the "workers" if you have more cores in your computer. Always use (available cores) - 1 or less cores so that the computer doesn't hang. Several other functions including FindClusters() benefit for parallelization, so it is a good idea to have it enabled if possible.


```r
library(future)

# check the current active plan
plan()

# change the current plan to access parallelization
plan("multiprocess", workers = 4)
plan()
```

The latest version of sctransform also supports using glmGamPoi package which substantially improves the speed of the learning procedure. It can be invoked by specifying method="glmGamPoi".

```r
library(glmGamPoi)
pbmc <- SCTransform(pbmc, method = "glmGamPoi", verbose = FALSE)
```
Depending on the variable genes and number of cells, the resulting Seurat object might be too large. You can increase the memory given to R by allocating more bytes:


```r
options(future.globals.maxSize= 2097152000) # 2Gb
```
This will give 2Gb of maximum memory to objects. The calculation is (needed megabytes)*1024^2 = bytes.

</details>
\ 

## Regressing "unwanted" variation, batch correction and integration

When performing our single cell analyses we want to compare gene abundance and expression between different cell types and conditions. To do this, will come across two different problems:

1. Each sequenced library might present certain characteristics that are driving part of the variance in the population but that are of no interest to our biological question. For example, some cells might be clustering by mitochondrial percentage because those particular cells were more stressed while being processed for sequencing. Therefore, we might want to regress out that unwanted variation so that we know that downstream comparisons won't be driven by this factor.
2. We often will have batches of cells that were sequenced at different times, for various reasons. They can be replicates of iPSC-differentiated cells from the same samples differentiated at separate times, they might be samples that have been re-sequenced with different methods, or they can be the same cell types but coming from different labs. To be able to cluster similar cell types together and compare them to those that are different, we want to remove the variation that is due to these batch effects.

Both situations come with their own issues and controversies. There's no agreement that we should regress mitochondrial percentage or cell cycle genes by default, and some people argue that it's better to simply annotate the clusters driven by these factors, and be aware of their differences, as there are biological reasons why some cells my be proliferating (in the case of cell cycle clusters) or have more mitochondrial genes (as detailed in the #qc section). The same issue applies to any other possible source of "unwanted" variation. You'll need to carefully evaluate your data and decide if your downstream analyses benefit from regressing out these covariates.

The second situation can be even trickier. Removing batch effects can mean also removing true biological information, particularly if your conditions and cells are not well-balanced across batches. 

### Removing "unwanted" variation

If you want to go ahead and correct for any covariate, you can do so within SCTransform() in "vars.to.regress":

```r
pbmc <- SCTransform(pbmc, 
                    vars.to.regress = "percent.mt", 
                    verbose = FALSE)
```
<details>
  <summary>**Calculating cell cycle score**</summary>
  
  We assign scores in the CellCycleScoring function, which stores S and G2/M scores in object meta data, along with the predicted classification of each cell in either G2M, S or G1 phase. CellCycleScoring can also set the identity of the Seurat object to the cell-cycle phase by passing set.ident = TRUE (the original identities are stored as old.ident). Please note that Seurat does not use the discrete classifications (G2M/G1/S) in downstream cell cycle regression (although those can be used for visualization in UMAP plots, for example). Instead, it uses the quantitative scores for G2M and S phase.


```r
## Loading a list of cell cycle genes compiled by the Regev lab
cc.genes <- readLines("data/resources/regev_lab_cell_cycle_genes_Kowalczyk_2015.txt")
S.genes <- cc.genes[1:43]
G2M.genes <- cc.genes[44:97]


pbmc <- CellCycleScoring(pbmc, s.features=S.genes, g2m.features=G2M.genes, set.ident=TRUE)
```

```
## Warning: The following features are not present in the object: DTL, UHRF1, MLF1IP, CDC6, EXO1,
## CASP8AP2, BRIP1, E2F8, not searching for symbol synonyms
```

```
## Warning: The following features are not present in the object: FAM64A, CCNB2, BUB1, HJURP, CDCA3,
## TTK, CDC25C, KIF2C, DLGAP5, CDCA2, CDCA8, ECT2, ANLN, NEK2, GAS2L3, not searching for symbol
## synonyms
```
</details>
\ 
If there are several variables to regress, it is advisable to remove all of them at the same time:


```r
pbmc <- SCTransform(pbmc, 
                    vars.to.regress = c("S.Score","G2M.Score", "percent.mt"), 
                    verbose = FALSE)
```

### Batch correction and integration

#### IntegrateData
Seurat has a pipeline to integrate data from different batches. These methods first identify cross-dataset pairs of cells that are in a matched biological state (‘anchors’), can be used both to correct for technical differences between datasets (i.e. batch effect correction), and to perform comparative scRNA-seq analysis of across experimental conditions.  Specifically, it uses canonical correlation analysis (CCA) to project the data into a subspace to identify correlations across datasets. It then computes mutual nearest neighbors (MNN) in the CCA subspace and serve as “anchors” to correct the data. 

To ilustrate this, we can use Seurat's example data:


```r
# load dataset after downloading the source: https://seurat.nygenome.org/src/contrib/ifnb.SeuratData_3.0.0.tar.gz
#install.packages("data/resources/ifnb.SeuratData_3.0.0.tar", repos = NULL, type = "source")
library(ifnb.SeuratData)
library(SeuratWrappers)
data("ifnb")

# split the dataset into a list of two seurat objects (stim and CTRL)
#ifnb.list <- SplitObject(ifnb, split.by = "stim")
# Doesn't work, separate merged object

ifnb.list <- lapply(X = ifnb.list, FUN = SCTransform)
features <- SelectIntegrationFeatures(object.list = ifnb.list, nfeatures = 3000)
ifnb.list <- PrepSCTIntegration(object.list = ifnb.list, anchor.features = features)
immune.anchors <- FindIntegrationAnchors(object.list = ifnb.list, normalization.method = "SCT", 
    anchor.features = features)
immune.combined.sct <- IntegrateData(anchorset = immune.anchors, normalization.method = "SCT")
```
Keep in mind that datasets to integrate need to be normalised in advance, using SCTransform() or any other function. You can select more features to integrate, which means that they will be found downstream in the integrated object, but this vastly increases computation time and size of the integrated object.
\n
It's also better to correct only if necessary, so always try merging data first using the merge() function and see how the data looks like. Often datasets that only differ in minor technical details will not show batch effects. Contrary to the integration pipeline, SCTransform() should be applied after merging, because merge() erases any normalization slots (THIS USED TO HAPPEN, BUT CHECK).


```r
PTK2B_merged <- merge(input_PTK2B_lib1, input_PTK2B_lib2,
                      add.cell.ids=c("input_PTK2B_lib1","input_PTK2B_lib1"))
PTK2B_merged <- SCTransform(PTK2B_merged, verbose = FALSE)
```


#### Harmony

[Harmony](https://github.com/immunogenomics/harmony) is a widely used alternative to Seurat default batch correction methods. In a [benchmark study](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-019-1850-9), both produced similar results but Harmony run faster.

Harmony first employs PCA for dimensionality reduction. In the PCA space, Harmony iteratively removes batch effects present. At each iteration, it clusters similar cells from different batches while maximizing the diversity of batches within each cluster and then calculates a correction factor for each cell to be applied. This approach is fast and can accurately detect the true biological connection across datasets.


```r
library(devtools)
install_github("immunogenomics/harmony")
library(harmony)

data("ifnb")

#ifnb.list <- SplitObject(ifnb, split.by = "stim")
for (i in 1:length(ifnb.list)) {
    ifnb.list[[i]] <- SCTransform(ifnb.list[[i]], verbose = FALSE)
}
ifnb.features <- SelectIntegrationFeatures(object.list = ifnb.list, nfeatures = 3000)
pancreas_merged <- merge(celseq, y = c(pancreas.list),  project = "pancreas", merge.data = TRUE)
VariableFeatures(ifnb_merged) <- ifnb.features
pancreas_merged <- RunPCA(object = pancreas_merged, assay = "SCT", npcs = 50)
pancreas_merged <- RunHarmony(object = pancreas_merged,
                                    assay.use = "SCT",
                                    reduction = "pca",
                                    dims.use = 1:50,
                                    group.by.vars = "tech",
                                    plot_convergence = TRUE)
pancreas_merged <- RunUMAP(object = pancreas_merged, assay = "SCT", reduction = "harmony", dims = 1:50)
pancreas_merged <- FindNeighbors(object = pancreas_merged, assay = "SCT", reduction = "harmony", dims = 1:50)
pancreas_merged <- FindClusters(object = pancreas_merged, resolution = 0.4)
```

One can also try not using SCTransform before Harmony:


```r
NormalizeData(verbose = FALSE) %>%
    FindVariableFeatures(selection.method = "vst", nfeatures = 2000) %>% 
    ScaleData(verbose = FALSE) %>% 
    RunPCA(pc.genes = pbmc@var.genes, npcs = 20, verbose = FALSE)
```

Just remember that Harmony used PCAs, so RunPCA() needs to be run before Harmony (unlike with the standard integration pipeline from Seurat, in which the PCA is performed afterwards).
PROVIDE EXAMPLE PLOTS OF THESE

\n
Integration of very different cell types requires more complex approaches, and will be reviewed later.

## References
- [Overview of normalisation methods](https://towardsdatascience.com/how-to-normalize-single-cell-a438281ea654)
- [Benchmark of batch correction methods](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-019-1850-9)

<!--chapter:end:03-method.Rmd-->

# Further reading {#application}

 - [Exhaustive course](http://bioconductor.org/books/release/OSCA/doublet-detection.html) using the *singleCellExperiment* class (instead of Seurat).
 x

<!--chapter:end:04-application.Rmd-->

# Final Words

We have finished a nice book.

<!--chapter:end:05-summary.Rmd-->


# References {-}


<!--chapter:end:06-references.Rmd-->

